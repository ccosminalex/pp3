﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PP3
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            double a, b, c;
            Console.Write("Introduceti n: ");
            n = int.Parse(Console.ReadLine());
            double[] v1 = new double[n];
            double[] v2 = new double[0];
            Console.WriteLine("Introduceti " + n + " numere: ");
            int i = 0;
            while (i < n)
            {
                v1[i] = double.Parse(Console.ReadLine());
                i++;
            }
            Console.Write("Introduceti a: ");
            a = double.Parse(Console.ReadLine());
            Console.Write("Introduceti b: ");
            b = double.Parse(Console.ReadLine());
            if (b < a)
            {
                c = a;
                a = b;
                b = c;
            }
            foreach (double nr in v1)
            {
                if(nr < a || nr > b)
                {
                    Array.Resize(ref v2, v2.Length + 1);
                    v2[v2.Length - 1] = nr;
                }
            }
            Console.WriteLine("Nr valorilor din vector care nu sunt cuprinse intre a si b: " + v2.Length);
            Console.ReadKey();
        }
    }
}
